package com.jlr.ica.app5.LayoutClasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by luke on 22/02/2016.
 */
public class GradientLine extends View {
    Context context;
    Paint paint1;
    Paint paint2;


    public GradientLine(Context context) {
        super(context);
        this.context = context;

    }

    public GradientLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();
        float cx = x / 2;
        float cy = y / 2;

        if (paint2==null) {
            Shader shader = new LinearGradient(cx, 0, x, 0, Color.WHITE,
                    Color.TRANSPARENT, Shader.TileMode.CLAMP);
            paint2 = new Paint();
            paint2.setFlags(Paint.ANTI_ALIAS_FLAG);
            paint2.setStyle(Paint.Style.FILL);
            paint2.setShader(shader);
        }
        canvas.drawRect(cx,0,x,y,paint2);

        if (paint1==null) {
            Shader shader = new LinearGradient(0, 0, cx, 0, Color.TRANSPARENT,
                    Color.WHITE,
                     Shader.TileMode.CLAMP);
            paint1 = new Paint();
            paint1.setFlags(Paint.ANTI_ALIAS_FLAG);
            paint1.setStyle(Paint.Style.FILL);
            paint1.setShader(shader);
        }
        canvas.drawRect(0,0,cx,y,paint1);



    }

    public void startAnimation(){
        final Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

            }
        }, 10, 10);
    }
}