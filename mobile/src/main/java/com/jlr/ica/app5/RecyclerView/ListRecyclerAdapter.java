package com.jlr.ica.app5.RecyclerView;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.jlr.ica.app5.DataClasses.ActionData;
import com.jlr.ica.app5.LayoutClasses.GestureVisual;
import com.jlr.ica.app5.MessageActivity;
import com.jlr.ica.app5.R;

import java.util.ArrayList;

public class ListRecyclerAdapter extends RecyclerView.Adapter<ListRecyclerAdapter.ViewHolder> implements View.OnClickListener{
    private ArrayList<Integer> mDataset;
    private Context context;
    private boolean connected;


    @Override
    public void onClick(View v) {
        ViewHolder holder = (ViewHolder) v.getTag();
        int i = holder.getAdapterPosition();

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public FrameLayout frameLayout;
        public ImageView gestureIcon;
        public Spinner spinner;
        public ImageButton actionConfig;
        public CheckBox checkBox;
        public ViewHolder(FrameLayout v) {
            super(v);
            frameLayout = v;
            gestureIcon = (ImageView) frameLayout.findViewById(R.id.gesture_icon);
            spinner = (Spinner) frameLayout.findViewById(R.id.spinner);
            actionConfig = (ImageButton) frameLayout.findViewById(R.id.action_config_button);
            checkBox = (CheckBox) frameLayout.findViewById(R.id.checkbox);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListRecyclerAdapter(ArrayList myDataset, Context c, boolean connected) {
        mDataset = myDataset;
        context = c;
        this.connected = connected;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        FrameLayout v = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gesture_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        // Sets the click adapter for the entire cell
        // to the one in this class.
        vh.itemView.setOnClickListener(ListRecyclerAdapter.this);
        vh.itemView.setTag(vh);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Integer id = mDataset.get(position);
        holder.gestureIcon.setImageDrawable(ContextCompat.getDrawable(context, ActionData.gestureIcons[position]));
        holder.gestureIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout demoLayout = (LinearLayout) LayoutInflater.from(context)
                        .inflate(R.layout.gesture_demo, holder.frameLayout, false);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(ActionData.gestureDesc[position]).setView(demoLayout)
                        .show();

                GestureVisual gestureVisual = (GestureVisual) demoLayout.findViewById(R.id.gesture_visual);
                gestureVisual.setGesture(position);
                gestureVisual.startAnim(500, 500);
            }
        });

        boolean enabled = ((MessageActivity) context).isGestureEnabled(position);
        holder.checkBox.setChecked(enabled);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((MessageActivity)context).setGestureEnabled(position, isChecked);
                updateRow(holder, position);
            }
        });

        holder.spinner.setAdapter(new CustomSpinnerAdapter(context, R.layout.gesture_spinner_row, ActionData.actionInt));
        holder.spinner.setSelection(id);
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                mDataset.set(position, pos);
                ((MessageActivity)context).changeGestureAction();
                updateRow(holder, position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        updateRow(holder, position);


    }

    private void updateRow(ViewHolder holder, int position) {
        Integer id = mDataset.get(position);
        boolean enabled = ((MessageActivity) context).isGestureEnabled(position);
        if (!enabled) {
            holder.frameLayout.setAlpha(0.5f);
        } else {
            holder.frameLayout.setAlpha(1.0f);
        }
        if (connected){
            holder.actionConfig.setVisibility(View.GONE);
            holder.spinner.setBackground(null);
            holder.spinner.setEnabled(false);
        } else {
            holder.actionConfig.setVisibility(View.VISIBLE);
            holder.actionConfig.setEnabled(true);
            holder.actionConfig.setAlpha(1.0f);
            switch (id) {
                case 1:
                    holder.actionConfig.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MessageActivity) context).appSwitchSettings();
                        }
                    });
                    break;
                default:
                    holder.actionConfig.setEnabled(false);
                    holder.actionConfig.setAlpha(0.5f);
                    break;
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}