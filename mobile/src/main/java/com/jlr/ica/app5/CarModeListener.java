package com.jlr.ica.app5;

import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;

/**
 * Created by luke on 22/03/2016.
 * Listens to broadcast changes in connectivity state
 * Wait some time and then check what phone is connected to
 * If connected, start service on watch
 */
public class CarModeListener extends BroadcastReceiver {

    public static final String TAG = "CarModeListener";
    public static final String START_SERVICE = "/start_service";
    public static final String STOP_SERVICE = "/stop_service";
    private Context mContext;
    int delay = 2000; // Delay found via testing

    @Override
    public void onReceive(Context context, final Intent intent) {
        mContext = context;
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "UIDelay:" + ((UiModeManager) mContext.getSystemService(Context.UI_MODE_SERVICE)).getCurrentModeType());
                boolean isConnected = (Configuration.UI_MODE_TYPE_CAR ==
                ((UiModeManager) mContext.getSystemService(Context.UI_MODE_SERVICE)).getCurrentModeType());
                if (isConnected) {
                    //Log.d(TAG, "Connected"+intent.toString());
                    String message = "0.7"; // sensitivity of gesture recognition. Default of 0.7 when connecting.
                    new SendToDataLayerThread(START_SERVICE, message,mContext).start();
                } else {
                    String message = "stop";
                    //Log.d(TAG, "Not Conne"+intent.toString());
                    new SendToDataLayerThread(STOP_SERVICE, message,mContext).start();
                }
            }
        },delay);
    }

}
