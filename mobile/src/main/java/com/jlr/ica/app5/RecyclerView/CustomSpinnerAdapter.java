package com.jlr.ica.app5.RecyclerView;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.jlr.ica.app5.DataClasses.ActionData;
import com.jlr.ica.app5.R;

/**
 * Created by luke on 23/03/2016.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<Integer> {

    Context context;
    public CustomSpinnerAdapter(Context context, int textViewResourceId,
                                Integer[] objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
// TODO Auto-generated constructor stub
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
// TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
//return super.getView(position, convertView, parent);

        FrameLayout row = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gesture_spinner_row, parent, false);

        ImageView actionIcon = (ImageView) row.findViewById(R.id.action_icon);
        TextView actionDesc = (TextView) row.findViewById(R.id.action_description);
        actionIcon.setImageDrawable(ContextCompat.getDrawable(context, ActionData.icons[position]));
        actionDesc.setText(ActionData.description[position]);


        return row;
    }
}