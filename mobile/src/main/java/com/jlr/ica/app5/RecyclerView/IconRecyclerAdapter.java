package com.jlr.ica.app5.RecyclerView;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jlr.ica.app5.DataClasses.AppData;
import com.jlr.ica.app5.R;

import java.util.ArrayList;

public class IconRecyclerAdapter extends RecyclerView.Adapter<IconRecyclerAdapter.ViewHolder> implements View.OnClickListener{
    private ArrayList<Integer> mDataset;
    private Context context;


    @Override
    public void onClick(View v) {
        ViewHolder holder = (ViewHolder) v.getTag();
        int i = holder.getAdapterPosition();

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout frameLayout;
        public ImageView icon;
        public ViewHolder(RelativeLayout v) {
            super(v);
            frameLayout = v;
            icon = (ImageView) frameLayout.findViewById(R.id.icon);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public IconRecyclerAdapter(ArrayList myDataset, Context c, int listType) {
        mDataset = myDataset;
        context = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public IconRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.icon_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        // Sets the click adapter for the entire cell
        // to the one in this class.
        vh.itemView.setOnClickListener(IconRecyclerAdapter.this);
        vh.itemView.setTag(vh);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Integer id = mDataset.get(position);
        holder.icon.setImageDrawable(ContextCompat.getDrawable(context, AppData.icons[id]));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}