package com.jlr.ica.app5.LayoutClasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.jlr.ica.app5.R;

/**
 * Created by luke on 23/03/2016.
 */
public class RoundedCorner extends View {
    Context mContext;

    public RoundedCorner(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public RoundedCorner(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float w = getWidth()/2;
        float h = getHeight()/2;
        float d = w;
        if (h<d)
            d=h;
        float r = 0.4f*d;


        Paint paint = new Paint();
        paint.setColor(ContextCompat.getColor(mContext, R.color.highlight));
        paint.setStyle(Paint.Style.FILL);

        canvas.drawRoundRect(w-d,h-d,w+d,h+d,r,r,paint);

    }
}
