package com.jlr.ica.app5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bosch.myspin.serversdk.MySpinException;
import com.bosch.myspin.serversdk.MySpinServerSDK;
import com.jlr.ica.app5.DataClasses.AppData;
import com.jlr.ica.app5.LayoutClasses.RoundedCorner;

/**
 * Created by luke on 22/03/2016.
 */
public class InterstitialActivity extends AppCompatActivity {
    LinearLayout cursorHolder;
    LinearLayout iconHolder;
    View contentMain;
    public static final String TAG = "InterstitialActivity";
    int animDur = 200;
    int px;
    int currentApp;
    int actionType = 0;
    public static final String ACTION_TYPE = "actionType";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interstitial_layout);
        cursorHolder = (LinearLayout) findViewById(R.id.cursor_holder);
        iconHolder = (LinearLayout) findViewById(R.id.icon_holder);
        contentMain = findViewById(R.id.content_main);
        contentMain.setVisibility(View.INVISIBLE);

        initMySpin();

        //generateView();


        Resources r = getResources();
        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, r.getDisplayMetrics());


    }

    private void initMySpin(){
        //Register Application with the mySPIN server
        try {
            MySpinServerSDK.sharedInstance().registerApplication(getApplication());
        } catch (MySpinException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        contentMain.setVisibility(View.INVISIBLE);
        generateView();
    }



    private void generateView() {
        cursorHolder.removeAllViews();
        iconHolder.removeAllViews();
        contentMain.setVisibility(View.VISIBLE);

        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        actionType = settings.getInt(ACTION_TYPE, 0);

        switch (actionType){
            case 0:
                gotoHomeActivity();
                break;
            case 1:
                generateAppSwitchView(settings);
                break;
            case 2:
                generateActionIcon();
                break;
            case 3:
                generateActionIcon();
                break;
            case 4:
                generateActionIcon();
                break;
        }
        resetActivitySettings();
    }

    private void gotoHomeActivity() {
        Intent intent = new Intent(this, MessageActivity.class);
        intent.setAction(Long.toString(System.currentTimeMillis()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void generateActionIcon(){
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int volume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

        final TextView gestureName = (TextView) findViewById(R.id.gesture_name);

        ImageView icon = new ImageView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        icon.setLayoutParams(params);
        switch (actionType){
            case 2:
                if (volume==0) {
                    icon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.mute));
                    gestureName.setText("Mute");

                } else {
                    icon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.raise_volume));
                    gestureName.setText("Unmute");
                }
                break;
            case 3:
                icon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.raise_volume));
                gestureName.setText("Raise Volume");
                break;
            case 4:
                icon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.lower_volume));
                gestureName.setText("Lower Volume");
                break;
        }

        icon.setPadding(px, px, px, px);
        iconHolder.addView(icon);

        ViewTreeObserver vto = iconHolder.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                iconHolder.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                contentMain.setVisibility(View.INVISIBLE);
                                gestureName.setText("");
                                finishAffinity();
                            }
                        });
                    }
                }, 1000);
            }
        });
    }

    private void generateAppSwitchView(SharedPreferences settings) {
        int numApps = settings.getInt("appNum", 0);
        Log.d(TAG, "appNum" + numApps);
        currentApp = settings.getInt("CurrentAppLS", -1);
        final int prevApp;
        if (numApps == 1) {
            prevApp = currentApp;
        } else {
            prevApp = settings.getInt("PrevAppLS", -1);
        }

        final TextView gestureName = (TextView) findViewById(R.id.gesture_name);
        gestureName.setText("Switching to "+AppData.names[settings.getInt("app" + currentApp, 0)]);

        ImageView icon;
        final RoundedCorner cursorPrev = new RoundedCorner(this);
        final RoundedCorner cursorCurrent = new RoundedCorner(this);

        View dummyView;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        for (int i = 0; i < numApps; i++) {
            // icon
            icon = new ImageView(this);
            icon.setLayoutParams(params);
            icon.setImageDrawable(ContextCompat.getDrawable(this, AppData.icons[settings.getInt("app" + i, 0)]));
            icon.setPadding(px, px, px, px);
            iconHolder.addView(icon);

            // cursor
            if (i == prevApp) {
                cursorPrev.setLayoutParams(params);
                cursorHolder.addView(cursorPrev);
            } else if (i == currentApp) {
                cursorCurrent.setLayoutParams(params);
                cursorHolder.addView(cursorCurrent);
            } else {
                dummyView = new View(this);
                dummyView.setLayoutParams(params);
                cursorHolder.addView(dummyView);
            }
        }
        cursorCurrent.setVisibility(View.INVISIBLE);
        if (numApps == 1) {
            // no animation
        } else {
            isVisible(cursorPrev);
            ViewTreeObserver vto = cursorCurrent.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    cursorCurrent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    isVisible(cursorPrev);
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //isVisible(cursorPrev);
                                    cursorPrev.setVisibility(View.INVISIBLE);
                                    cursorCurrent.setVisibility(View.VISIBLE);
                                    cursorPrev.invalidate();
                                    cursorCurrent.invalidate();
                                    launchApp();
                                }
                            });
                        }
                    }, 1000);
                }
            });
        }
        /*if (numApps==1){
            // no animation
        } else {
            ViewTreeObserver vto = cursorHolder.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    cursorHolder.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    final int aninDur = 200;
                    final int aninDelay = 50;
                    if (prevApp >= 0) {
                        int delta = currentApp - prevApp;
                        final int dir;
                        if (delta == 1 || delta < -1) {
                            dir = 1;
                        } else {
                            dir = -1;
                        }
                        cursorCurrent.setIsCurrent(true);
                        Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                cursorPrev.startAnim(dir, aninDur, aninDelay, false);
                                cursorCurrent.startAnim(-dir, aninDur, aninDelay, true);
                            }
                        },50);
                    }

                }
            });
            }*/
        /*if (numApps==1){
            // no animation
        } else {
            ViewTreeObserver vto = cursorHolder.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    cursorHolder.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (prevApp >= 0) {
                                int delta = currentApp - prevApp;
                                int dir = -1;
                                if (delta == 1 || delta < -1) {
                                    dir = -dir;
                                }
                                final float transX = dir * cursor.getWidth();
                                if ((currentApp != 0 && dir == 1) || (prevApp != 0 && dir == -1)) {
                                    Animation animation = new TranslateAnimation(0, transX, 0, 0);
                                    animation.setDuration(animDur);
                                    animation.setFillAfter(true);
                                    cursor.startAnimation(animation);
                                } else {
                                    final int offset;
                                    if (dir == 1) {
                                        offset = prevApp;
                                    } else {
                                        offset = currentApp;
                                    }

                                    Animation animation = new TranslateAnimation(0, transX * 2, 0, 0);
                                    animation.setDuration(animDur / 2);
                                    animation.setFillAfter(true);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            Animation animation2 = new TranslateAnimation(-transX * (offset + 2), -transX * offset, 0, 0);
                                            animation2.setDuration(animDur / 2);
                                            animation2.setFillAfter(true);
                                            cursor.startAnimation(animation2);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {
                                        }
                                    });
                                    cursor.startAnimation(animation);
                                }
                            }
                        }
                    }, 200);


                }
            });
        }*/
    }

    private void isVisible(View v) {
        Rect scrollBounds = new Rect();
        cursorHolder.getHitRect(scrollBounds);
        if (v.getLocalVisibleRect(scrollBounds)) {
            Log.d("visible", "true");
        } else {
            Log.d("visible", "false");
        }
    }

    private void launchApp() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                contentMain.setVisibility(View.INVISIBLE);
                SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
                final String appLaunch = AppData.packageIds[settings.getInt("app" + currentApp, 0)];
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(appLaunch);
                startActivity(launchIntent);
            }
        }, 1000);
    }

    private void resetActivitySettings(){
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(ACTION_TYPE, 0);
        editor.commit();
    }
}
