package com.jlr.ica.app5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.WearableListenerService;
import com.jlr.ica.app5.DataClasses.AppData;

import java.util.ArrayList;

/**
 * Created by luke on 18/03/2016.
 * on message received from mobile;
 * update SharedPrefs with gesture info;
 * carry-out action (e.g. mute, volume);
 * launch InterstitialActivity;
 */
public class MobileListenerService extends WearableListenerService {

    ArrayList<String> appList;
    int currentApp;

    public static final String TAG = "MobileListenerService";
    public static final String CURRENT_APP = "CurrentAppLS";
    public static final String PREV_APP = "PrevAppLS";
    public static final String ACTION_TYPE = "actionType";


    ArrayList<Integer> gestureItems;
    ArrayList<Boolean> gesturesEnabled;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Created");
        recallAppList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroyed");
    }

    private void recallAppList() {
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        currentApp = settings.getInt(CURRENT_APP, 0);
        int numApps = settings.getInt("appNum", 0);
        appList = new ArrayList<>();
        for (int i = 0; i<numApps; i++) {
            int appId = settings.getInt("app" + i, 0);
            appList.add(AppData.packageIds[appId]);
        }

    }

    private void getGesturePrefs() {
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        int numGesture = settings.getInt("gestureNum", 0);
        Log.d(TAG, "gestureNum" + numGesture);
        gesturesEnabled = new ArrayList<>();
        gestureItems = new ArrayList<>();
        for (int i = 0; i<numGesture; i++) {
            gestureItems.add(settings.getInt("gesture" + i, 0));
            gesturesEnabled.add(settings.getBoolean("gestureEn" + i, false));
        }
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDataChanged: " + dataEvents + " for " + getPackageName());
        }
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_DELETED) {
                Log.i(TAG, event + " deleted");
            } else if (event.getType() == DataEvent.TYPE_CHANGED) {
                final String message = DataMap.fromByteArray(event.getDataItem().getData()).get("gesture");
                final char gesture = message.charAt(0);
                getGesturePrefs();
                switch (gesture){
                    case 'L':
                        if (gesturesEnabled.get(0)) {
                            Log.d(TAG, "Gesture Left");
                            triggerGesture(gestureItems.get(0),-1);
                        }
                        break;
                    case 'R':
                        if (gesturesEnabled.get(1)) {
                            Log.d(TAG, "Gesture Right");
                            triggerGesture(gestureItems.get(1),1);
                        }
                        break;
                    case 'M':
                        Log.d(TAG, "Gesture Mute");
                        if (gesturesEnabled.get(2)) {
                            Log.d(TAG, "Gesture Left");
                            triggerGesture(gestureItems.get(2),0);
                        }
                        break;
                    case 'U':
                        Log.d(TAG, "Gesture Up");
                        if (gesturesEnabled.get(3)) {
                            Log.d(TAG, "Gesture Left");
                            triggerGesture(gestureItems.get(3),-1);
                        }
                        break;
                    case 'D':
                        Log.d(TAG, "Gesture Down");
                        if (gesturesEnabled.get(4)) {
                            Log.d(TAG, "Gesture Left");
                            triggerGesture(gestureItems.get(4),1);
                        }
                        break;
                }
            }
        }
    }

    private void triggerGesture(int id,int dir){
        switch (id){
            case 0:
                Log.d(TAG, "Action Do nothing");
                break;
            case 1:
                switch (dir){
                    case -1:
                        Log.d(TAG, "Action App Left");
                        launchAppId(-1);
                        break;
                    default:
                        Log.d(TAG, "Action App Right");
                        launchAppId(1);
                        break;
                }
                break;
            case 2:
                Log.d(TAG, "Action Mute");
                sendLaunchIntent(2);
                AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_TOGGLE_MUTE, AudioManager.FLAG_PLAY_SOUND);
                break;
            case 3:
                Log.d(TAG, "Action Raise Volume");
                sendLaunchIntent(3);
                ((AudioManager) getSystemService(Context.AUDIO_SERVICE)).adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
                break;
            case 4:
                sendLaunchIntent(4);
                Log.d(TAG, "Action Lower Volume");
                ((AudioManager) getSystemService(Context.AUDIO_SERVICE)).adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
                /*Intent i = new Intent(Intent.ACTION_MEDIA_BUTTON);
                i.putExtra(Intent.EXTRA_KEY_EVENT,new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PAUSE));
                sendOrderedBroadcast(i, null);
                i = new Intent(Intent.ACTION_MEDIA_BUTTON);
                i.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PAUSE));
                sendOrderedBroadcast(i, null);*/
                //((PackageManager)getPackageManager()).getReceiverInfo(C)
                break;
            case 5:
                Log.d(TAG, "Show home screen");
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(AppData.launcherId);
                startActivity(launchIntent);
                break;
            case 6:
                Log.d(TAG, "Show settings");
                final String settings = "com.jlr.ica.app5";
                Intent settingsIntent = getPackageManager().getLaunchIntentForPackage(settings);
                startActivity(settingsIntent);
                break;
        }
    }

    public void launchAppId(int idShift){

        recallAppList();
        int appN = appList.size();
        if (appN>0) {
            SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
            boolean historyEnabled = settings.getBoolean("historyEnabled",false);
            if (historyEnabled) {
                String currentPackage = MyPackageAnalyser.getTopAppName(this);
                Log.d(TAG, "appName: " + currentPackage);
                for (int i = 0; i<appList.size(); i++){
                    if (appList.get(i).equalsIgnoreCase(currentPackage)){
                        currentApp = i;
                        break;
                    }
                }
            }

            int prevApp = currentApp;
            currentApp += idShift;
            if (currentApp < 0) {
                currentApp = appN - 1;
            } else if (currentApp >= appN) {
                currentApp = 0;
            }


            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(CURRENT_APP, currentApp);
            editor.putInt(PREV_APP, prevApp);
            editor.commit();

            sendLaunchIntent(1);

            //Log.d(TAG, "currentAppPost" + currentApp);

            //final String appLaunch = appList.get(currentApp);


            //Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.jlr.ica.app5");
            //startActivity(launchIntent);

            //Interstitial



            /*Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage(appLaunch);
                    startActivity(launchIntent);
                }
            }, 1000);*/

        }
    }

    public void sendLaunchIntent(int type){
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(ACTION_TYPE, type);
        editor.commit();


        Intent dialogIntent = new Intent(this, InterstitialActivity.class);
        dialogIntent.setAction(Long.toString(System.currentTimeMillis()));
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }



}
