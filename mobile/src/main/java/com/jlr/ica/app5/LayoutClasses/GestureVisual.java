package com.jlr.ica.app5.LayoutClasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.jlr.ica.app5.InterstitialActivity;
import com.jlr.ica.app5.MessageActivity;
import com.jlr.ica.app5.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by luke on 05/04/2016.
 */
public class GestureVisual extends View{

    Context context;
    float aspectPoint = 426.0f/300.0f;
    float aspectFlat = 438.0f/367.0f;
    int w;
    int h;
    float cx;
    float cy;
    float angleStart = -20;
    float angleSweep = 50;
    float ratio = 0.0f;
    int gesture = 0;

    public GestureVisual(Context context) {
        super(context);
        this.context = context;
    }

    public GestureVisual(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        w = getWidth();
        h = getHeight();
        float dx;
        float dy;
        float offset;
        Drawable d;
        canvas.drawColor(Color.WHITE);

        switch (gesture){
            case 0:
                cx = 0.5f*w;
                cy = 0.5f*h;
                dx = 0.3f*w;
                dy = aspectFlat*dx;
                offset = -(-0.25f*w+ratio*0.5f*w);
                d = ContextCompat.getDrawable(context, R.drawable.swipe_3d);
                d.setBounds((int) (offset+cx - dx / 2), (int) (cy - dy / 2), (int) (offset+cx + dx / 2), (int) (cy + dy / 2));
                d.draw(canvas);
                break;
            case 1:
                cx = 0.5f*w;
                cy = 0.5f*h;
                dx = 0.3f*w;
                dy = aspectFlat*dx;
                offset = (-0.25f*w+ratio*0.5f*w);
                d = ContextCompat.getDrawable(context, R.drawable.swipe_3d);
                d.setBounds((int) (offset+cx - dx / 2), (int) (cy - dy / 2), (int) (offset+cx + dx / 2), (int) (cy + dy / 2));
                d.draw(canvas);
                break;
            case 2:
                cx = 0.4f*w;
                cy = 0.5f*h;
                dx = 0.3f*w;
                dy = aspectPoint*dx;
                canvas.rotate(angleStart + ratio * angleSweep, 0.8f * w, 0.8f * h);
                d = ContextCompat.getDrawable(context, R.drawable.mute_3d);
                d.setBounds((int) (cx - dx / 2), (int) (cy - dy / 2), (int) (cx + dx / 2), (int) (cy + dy / 2));
                d.draw(canvas);
                break;
        }



    }

    public void startAnim(int dur,int delay){
        int dt = 20;
        ratio = 0.0f;
        final float dr = ((float)dt)/((float)dur);
        final Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (ratio >= 1) {
                    ((MessageActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ratio = 1;
                            t.cancel();
                            invalidate();
                        }
                    });
                } else {

                    ((MessageActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ratio += dr;
                            invalidate();
                        }
                    });
                }
            }
        }, delay, dt);
    }

    public void setGesture(int gesture){
        this.gesture = gesture;
    }
}
