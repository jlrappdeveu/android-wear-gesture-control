package com.jlr.ica.app5.LayoutClasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.jlr.ica.app5.InterstitialActivity;
import com.jlr.ica.app5.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by luke on 23/03/2016.
 */
public class RoundedCornerAnim extends View {
    Context mContext;
    float ratio = 0.0f;
    int direction = 1;
    Paint paint;
    float w;
    float h;
    float d;
    float r;
    boolean isCurrent = false;

    public RoundedCornerAnim(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public RoundedCornerAnim(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        w = getWidth()/2;
        h = getHeight()/2;
        d = w;
        if (h<d)
            d=h;
        r = 0.4f*d;

        if (paint==null) {
            paint = new Paint();
            paint.setColor(ContextCompat.getColor(mContext, R.color.highlight));
            paint.setStyle(Paint.Style.FILL);
        }

        float ratioNorm = Math.max(0,ratio);
        if (isCurrent)
            ratioNorm = 1-ratioNorm;
        float offset = direction*ratioNorm*w*2;
        canvas.drawRoundRect(offset+w-d,h-d,offset+w+d,h+d,r,r,paint);

    }

    public void startAnim(int dir,int dur,int delay, boolean current){
        direction = dir;
        int dt = 20;
        ratio = -1.0f;
        isCurrent = current;
        final float dr = ((float)dt)/((float)dur);
        final Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (ratio>=1){
                    ((InterstitialActivity)mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ratio = 1;
                            t.cancel();
                            invalidate();
                        }
                    });
                } else {

                    ((InterstitialActivity)mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ratio += dr;
                            invalidate();
                        }
                    });
                }
            }
        },delay, dt);
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
        invalidate();
    }
}
