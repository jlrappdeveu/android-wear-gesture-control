package com.jlr.ica.app5.RecyclerView;

/**
 * Created by luke on 22/03/2016.
 */
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.jlr.ica.app5.DataClasses.AppData;
import com.jlr.ica.app5.MessageActivity;
import com.jlr.ica.app5.R;

import java.util.ArrayList;
import java.util.Collections;


/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private ArrayList<Integer> mDataset;
    private Context context;
    private TYPE type;
    public enum TYPE {
        SELECTED,
        UNSELECTED
    }

    private final OnStartDragListener mDragStartListener;

    public RecyclerListAdapter(Context context, OnStartDragListener dragStartListener, ArrayList myDataset, TYPE type) {
        mDragStartListener = dragStartListener;
        mDataset = myDataset;
        this.context = context;
        this.type = type;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.icon_view, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        itemViewHolder.itemView.setTag(itemViewHolder);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        final Integer id = mDataset.get(position);

        holder.buttonHolder.setVisibility(View.GONE);

        holder.icon.setImageDrawable(ContextCompat.getDrawable(context, AppData.icons[id]));
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.buttonHolder.setVisibility(View.VISIBLE);
                Animation fadeIn = new AlphaAnimation(0, 1);
                fadeIn.setInterpolator(new DecelerateInterpolator());
                fadeIn.setDuration(200);
                holder.buttonHolder.startAnimation(fadeIn);
            }
        });

        switch (type){
            case SELECTED:
                holder.dismiss.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.trash));
                break;
            case UNSELECTED:
                holder.dismiss.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.plus_box));
                break;
        }
        holder.dismiss.setEnabled(true);
        holder.dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ind = mDataset.indexOf(id);
                //Log.d("click", String.valueOf(ind));
                holder.dismiss.setEnabled(false);
                onItemDismiss(ind);
                switch (type){
                    case SELECTED:
                        ((MessageActivity) context).addToUnselected(id);
                        break;
                    case UNSELECTED:
                        ((MessageActivity) context).addToSelected(id);
                        break;
                }

            }
        });

        holder.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new DecelerateInterpolator());
                fadeOut.setDuration(200);
                holder.buttonHolder.startAnimation(fadeOut);
                holder.buttonHolder.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onItemDismiss(int position) {
        try {
            mDataset.remove(position);
        } catch (Exception e){

        }
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mDataset, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        ((MessageActivity) context).updateSharedPrefsAppOrder();
        return true;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final ImageView icon;
        public final View iconHolder;
        public final View buttonHolder;
        public final ImageView confirm;
        public final ImageView dismiss;

        public ItemViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            iconHolder = (View) itemView.findViewById(R.id.icon_holder);
            buttonHolder = itemView.findViewById(R.id.button_holder);
            confirm = (ImageView) itemView.findViewById(R.id.confirm);
            dismiss = (ImageView) itemView.findViewById(R.id.dismiss);
        }

        @Override
        public void onItemSelected() {
            iconHolder.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            iconHolder.setBackgroundColor(0);
        }
    }
}