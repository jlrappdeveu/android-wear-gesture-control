package com.jlr.ica.app5.DataClasses;

import com.jlr.ica.app5.R;

/**
 * Created by luke on 21/03/2016.
 */
public class ActionData {

    public final static String[] description = {
            "Do Nothing",
            "Cycle through favourite apps",
            "Toggle mute",
            "Raise volume",
            "Lower volume",
            "Home to app grid",
            "Show enabled gestures"
    };

    public final static int[] icons = {
            R.drawable.trash,
            R.drawable.app_switch,
            R.drawable.mute,
            R.drawable.raise_volume,
            R.drawable.lower_volume,
            R.drawable.home,
            R.drawable.settings
    };

    public final static int[] gestureIcons = {R.drawable.hand_left,
            R.drawable.hand_right,
            R.drawable.finger_gesture,
            R.drawable.hand_up,
            R.drawable.hand_down

    };

    public final static String[] gestureDesc = {"Swipe left",
            "Swipe right",
            "'Shush' gesture",
            "Swipe up",
            "Swipe down"

    };

    public final static Integer[] actionInt = {0,1,2,3,4,5,6};

    public final static int gestureNum = 5;

}
