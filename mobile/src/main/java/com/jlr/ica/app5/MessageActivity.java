package com.jlr.ica.app5;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.bosch.myspin.serversdk.MySpinException;
import com.bosch.myspin.serversdk.MySpinServerSDK;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.jlr.ica.app5.DataClasses.ActionData;
import com.jlr.ica.app5.DataClasses.AppData;
import com.jlr.ica.app5.RecyclerView.ListRecyclerAdapter;
import com.jlr.ica.app5.RecyclerView.OnStartDragListener;
import com.jlr.ica.app5.RecyclerView.RecyclerListAdapter;
import com.jlr.ica.app5.RecyclerView.SimpleItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.List;


public class MessageActivity extends AppCompatActivity
        implements
        MySpinServerSDK.ConnectionStateListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnStartDragListener {

    public static final String TAG = "MobileMessageActivity";

    GoogleApiClient googleClient;
    Button mButtonSend;
    Button mButtonInter;
    EditText mAccelSetting;

    int[] defaultAppIds = {0,1,3};
    public ArrayList<Integer> activeAppIds;
    public ArrayList<Integer> selectedAppIds;
    public ArrayList<Integer> unselectedAppIds;
    public int maxAppNum = 5;


    RecyclerView selectedRv;
    RecyclerListAdapter selectedRa;
    private ItemTouchHelper selectedItemTouchHelper;
    RecyclerView unselectedRv;
    RecyclerListAdapter unselectedRa;
    private ItemTouchHelper unselectedItemTouchHelper;
    Context mContext;

    Switch historySwitch;

    RecyclerView gestureRv;
    ListRecyclerAdapter gestureRa;
    ArrayList<Integer> gestureItems;
    ArrayList<Boolean> gesturesEnabled;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_disconnected);
        //MYSpin
        initMySpin();
        mContext = this;

        // Build a new GoogleApiClient that includes the Wearable API
        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        // Pick layout based on mySpin connection state
        setCorrectLayout();

        // Check that apps have been added
        try {
            SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
            int numApps = settings.getInt("appNum", -1);
            if (numApps<1){
                throw new Exception("no apps");
            }
        } catch (Exception e){
            SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
            SharedPreferences.Editor editor = settings.edit();
            for (int i = 0; i<defaultAppIds.length; i++) {
                // use the name as the key, and the icon as the value
                editor.putInt("app" + i, defaultAppIds[i]);
            }
            editor.putInt("appNum", defaultAppIds.length);
            editor.commit();
        }

        // Check that gestures have been addded
        try {
            SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
            int numGesture = settings.getInt("gestureNum", -1);
            if (numGesture!= ActionData.gestureNum){
                throw new Exception("no gestures");
            }
        } catch (Exception e){
            SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
            SharedPreferences.Editor editor = settings.edit();
            for (int i = 0; i<ActionData.gestureNum; i++) {
                // use the name as the key, and the icon as the value
                editor.putInt("gesture" + i, 0);
            }
            editor.putInt("gestureNum", ActionData.gestureNum);
            editor.commit();
            setCorrectLayout();
        }

    }

    private void updateInstalledApps() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final PackageManager pm = getPackageManager();
                //get a list of installed apps.
                List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

                activeAppIds = new ArrayList<>();
                for (int i = 0; i<AppData.packageIds.length; i++) {
                    for (ApplicationInfo packageInfo : packages) {
                        if (packageInfo.packageName.equalsIgnoreCase(AppData.packageIds[i])){
                            activeAppIds.add(i);
                        }
                    }
                }
            }
        }).start();

    }

    public void updateSharedPrefsAppOrder(){
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();

        for (int i = 0; i<selectedAppIds.size(); i++) {
            // use the name as the key, and the icon as the value
            editor.putInt("app" + i, selectedAppIds.get(i));
        }
        editor.putInt("appNum", selectedAppIds.size());
        editor.commit();
    }

    private void updateSharedPrefsGesture(){
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();

        for (int i = 0; i<gestureItems.size(); i++) {
            // use the name as the key, and the icon as the value
            editor.putBoolean("gestureEn" + i, gesturesEnabled.get(i));
            editor.putInt("gesture" + i, gestureItems.get(i));
        }
        editor.commit();
    }

    public void changeGestureAction(){
        updateSharedPrefsGesture();
    }

    public void setGestureEnabled(int id, Boolean enabled){
        gesturesEnabled.set(id,enabled);
        updateSharedPrefsGesture();
    }

    private void layoutConnected(){
        //Log.d(TAG, "UI:"+((UiModeManager) getSystemService(Context.UI_MODE_SERVICE)).getCurrentModeType());
        setContentView(R.layout.activity_connected);

        // toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setTitle("Active Gestures");

        getGesturePrefs();

        // recyclerView
        gestureRa = new ListRecyclerAdapter(gestureItems,this,true);
        gestureRv = (RecyclerView) findViewById(R.id.gesture_list);
        gestureRv.setHasFixedSize(true);
        gestureRv.setLayoutManager(new LinearLayoutManager(this));
        gestureRv.setAdapter(gestureRa);

        //appSwitchSettings();

    }

    private void getGesturePrefs() {
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        int numGesture = settings.getInt("gestureNum", 0);
        Log.d(TAG, "gestureNum" + numGesture);
        gesturesEnabled = new ArrayList<>();
        gestureItems = new ArrayList<>();
        for (int i = 0; i<numGesture; i++) {
            gestureItems.add(settings.getInt("gesture" + i, 0));
            gesturesEnabled.add(settings.getBoolean("gestureEn" + i, false));
        }
    }

    public boolean isGestureEnabled(int id){
        return gesturesEnabled.get(id);
    }

    public void appSwitchSettings() {
        setContentView(R.layout.customise_apps);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Configure App Switching");
        SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
        int numApps = settings.getInt("appNum", 0);
        Log.d(TAG, "appNum" + numApps);
        selectedAppIds = new ArrayList<>();
        for (int i = 0; i<numApps; i++) {
            selectedAppIds.add(settings.getInt("app" + i, 0));
        }
        unselectedAppIds = new ArrayList<>();
        for (Integer id: activeAppIds) {
            if (!selectedAppIds.contains(id)) {
                unselectedAppIds.add(id);
            }
        }

        // Upper recycler view showing current app cycle order
        selectedRa = new RecyclerListAdapter(this, this, selectedAppIds, RecyclerListAdapter.TYPE.SELECTED);
        selectedRv = (RecyclerView) findViewById(R.id.selected_recycler_view);
        selectedRv.setHasFixedSize(true);
        selectedRv.setAdapter(selectedRa);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
        selectedRv.setLayoutManager(gridLayoutManager);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(selectedRa);
        selectedItemTouchHelper = new ItemTouchHelper(callback);
        selectedItemTouchHelper.attachToRecyclerView(selectedRv);

        unselectedRa = new RecyclerListAdapter(this, this, unselectedAppIds, RecyclerListAdapter.TYPE.UNSELECTED);
        unselectedRv = (RecyclerView) findViewById(R.id.unselected_recycler_view);
        unselectedRv.setHasFixedSize(true);
        unselectedRv.setAdapter(unselectedRa);
        GridLayoutManager unGridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
        unselectedRv.setLayoutManager(unGridLayoutManager);
        ItemTouchHelper.Callback unCallback = new SimpleItemTouchHelperCallback(unselectedRa);
        unselectedItemTouchHelper = new ItemTouchHelper(unCallback);
        unselectedItemTouchHelper.attachToRecyclerView(unselectedRv);

        historySwitch = (Switch) findViewById(R.id.history_switch);
        boolean historyEnabled = settings.getBoolean("historyEnabled",false);
        final boolean historyPermission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String permissionCheck = MyPackageAnalyser.getTopAppName(this);
            historyPermission = permissionCheck.length()>0;
        } else {
            historyPermission = true;
        }
        historySwitch.setChecked(historyEnabled&&historyPermission);
        historySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences settings = getSharedPreferences(AppData.SHARED_PREF, 0);
                SharedPreferences.Editor editor = settings.edit();
                if (isChecked){
                    editor.putBoolean("historyEnabled",true);
                    editor.commit();
                    if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)&&!historyPermission) {
                        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                        startActivity(intent);
                    }
                } else {
                    editor.putBoolean("historyEnabled",false);
                    editor.commit();
                }
            }
        });

    }

    public void addToUnselected(Integer appId){
        unselectedAppIds.add(appId);
        unselectedRa.notifyItemInserted(unselectedAppIds.size() - 1);
        updateSharedPrefsAppOrder();
    }

    public void addToSelected(Integer appId){
        if (selectedAppIds.size()>=maxAppNum){
            Snackbar snackbar = Snackbar
                    .make((LinearLayout) findViewById(R.id.customise_app_layout), "Only 6 apps may be added", Snackbar.LENGTH_LONG);
            snackbar.show();
            unselectedAppIds.add(appId);
            unselectedRa.notifyItemInserted(unselectedAppIds.size() - 1);
        } else {
            selectedAppIds.add(appId);
            selectedRa.notifyItemInserted(selectedAppIds.size() - 1);
        }
        updateSharedPrefsAppOrder();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        selectedItemTouchHelper.startDrag(viewHolder);
    }

    private void layoutDisconnected(){
        setContentView(R.layout.activity_disconnected);
        mAccelSetting = (EditText) findViewById(R.id.accel_setting);

        mButtonSend = (Button) findViewById(R.id.send_button);
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGestureListener();
            }
        });

        mButtonInter = (Button) findViewById(R.id.inter_button);
        mButtonInter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialogIntent = new Intent(mContext, InterstitialActivity.class);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setTitle(R.string.app_name);

        // recyclerView
        getGesturePrefs();

        gestureRa = new ListRecyclerAdapter(gestureItems,this,false);
        gestureRv = (RecyclerView) findViewById(R.id.gesture_list);
        gestureRv.setHasFixedSize(true);
        gestureRv.setLayoutManager(new LinearLayoutManager(this));
        gestureRv.setAdapter(gestureRa);
    }

    public void startGestureListener(){
        String message = "0.7";
        new SendToDataLayerThread("/start_service", message).start();
    }
    public void stopGestureListener(){
        String message = "stop";
        new SendToDataLayerThread("/stop_service", message).start();
    }
    private void mySpinStartGesture(){
        if(MySpinServerSDK.sharedInstance().isConnected()) {
            Log.d(TAG, " : mySpinStartGesture() : isConnected");
            startGestureListener();
        }
        else
        {
            Log.d(TAG ," : mySpinStartGesture() : NOTconnected");
            stopGestureListener();
        }
    }

    // Connect to the data layer when the Activity starts
    @Override
    protected void onStart() {
        super.onStart();
        googleClient.connect();
        registerMySpin();
    }

    // Send a message when the data layer connection is successful.
    @Override
    public void onConnected(Bundle connectionHint) {

    }

    // Disconnect from the data layer when the Activity stops
    @Override
    protected void onStop() {
        if (null != googleClient && googleClient.isConnected()) {
            googleClient.disconnect();
        }
        unRegisterMySpin();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setCorrectLayout();
        updateInstalledApps();
    }

    // Placeholders for required connection callbacks
    @Override
    public void onConnectionSuspended(int cause) { }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) { }

    // Unused project wizard code
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case android.R.id.home:
                setCorrectLayout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnectionStateChanged(boolean b) {
        setCorrectLayout();
        mySpinStartGesture();
    }

    private void setCorrectLayout(){

        Log.d(TAG, " : setCorrectLayout()");
        if(MySpinServerSDK.sharedInstance().isConnected()) {
            Log.d(TAG, " : setCorrectLayout() : isConnected");
            layoutConnected();
        }
        else
        {
            Log.d(TAG ," : setCorrectLayout() : NOTconnected");
            layoutDisconnected();
        }
    }

    private void initMySpin(){
        //Register Application with the mySPIN server
        try {
            MySpinServerSDK.sharedInstance().registerApplication(getApplication());
        } catch (MySpinException e) {
            e.printStackTrace();
        }
    }

    private void registerMySpin(){
        try {
            MySpinServerSDK.sharedInstance().registerConnectionStateListener(this);
        } catch (MySpinException e) {
            e.printStackTrace();
        }
    }

    private void unRegisterMySpin() {
        // When this activity gets stopped unregister for mySPIN connection events.
        try {
            MySpinServerSDK.sharedInstance().unregisterConnectionStateListener(this);
        } catch (MySpinException e) {
            e.printStackTrace();
        }
    }

    class SendToDataLayerThread extends Thread {
        String path;
        String message;

        // Constructor to send a message to the data layer
        SendToDataLayerThread(String p, String msg) {
            path = p;
            message = msg;
        }

        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            for (Node node : nodes.getNodes()) {
                MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(googleClient, node.getId(), path, message.getBytes()).await();
                if (result.getStatus().isSuccess()) {
                    Log.v("myTag", "Message: {" + message + "} sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.v("myTag", "ERROR: failed to send Message");
                }
            }
        }
    }
}