package com.jlr.ica.app5.LayoutClasses;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by luke on 23/03/2016.
 */
public class ChevronView extends View {
    Context mContext;

    public ChevronView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public ChevronView(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float w = getWidth();
        float h = getHeight();
        float d = w*0.7f;

        Resources r = getResources();
        float radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, r.getDisplayMetrics());

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        paint.setAlpha(100);
        CornerPathEffect corEffect = new CornerPathEffect(radius);
        paint.setPathEffect(corEffect);
        Path path = new Path();
        path.moveTo(0, 0);
        path.lineTo(d, 0);
        path.lineTo(w, h/2);
        path.lineTo(d, h);
        path.lineTo(0, h);
        path.close();
        canvas.drawPath(path, paint);

    }
}
