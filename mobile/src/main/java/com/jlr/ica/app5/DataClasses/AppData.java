package com.jlr.ica.app5.DataClasses;

import com.jlr.ica.app5.R;

/**
 * Created by luke on 21/03/2016.
 */
public class AppData {

    public final static String[] packageIds = {"com.glympse.android.auto",
            "com.aupeo.AupeoNextGen",
            "com.audiobooks.androidapp",
            "com.wcities.hotelseeker",
            "com.wcities.cityseeker",
            "com.parkopedia",
            "com.rivet.app",
            "com.stitcher.app",
            "com.wcities.eventseeker"
    };

    public final static String[] names = {"Glympse",
            "AUPEO!",
            "Audiobooks",
            "hotelseeker",
            "cityseeker",
            "Parkopedia",
            "Rivet Radio",
            "Stitcher",
            "eventseeker"
    };

    public final static String launcherId = "com.bosch.myspin.launcherapp_jaguar";

    public final static int[] icons = {R.drawable.glympse,
            R.drawable.aupeo,
            R.drawable.audiobooks,
            R.drawable.hotelseeker,
            R.drawable.cityseeker,
            R.drawable.parkopedia,
            R.drawable.rivetradio,
            R.drawable.stitcher,
            R.drawable.eventseeker
    };

    public static final String SHARED_PREF = "SharedPrefLS";
}
