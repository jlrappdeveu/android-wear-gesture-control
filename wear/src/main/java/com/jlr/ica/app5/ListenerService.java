package com.jlr.ica.app5;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Wearable listener service for data layer messages
 * Created by luke on 22/03/2016.
 * Credit to michaelHahn on 1/11/15.
 *
 * Start or stop WearGestureService based on message
 */
public class ListenerService extends WearableListenerService{

    public static final String TAG = "wear/ListenerService";
    public static final String START_SERVICE = "/start_service";
    public static final String STOP_SERVICE = "/stop_service";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        if (messageEvent.getPath().equals(START_SERVICE)){
            Log.v(TAG, START_SERVICE);
            final String message = new String(messageEvent.getData());
            Log.v(TAG, "Sensitivity is: " + message);
            Intent intent = new Intent(getApplicationContext(), WearGestureService.class);
            intent.putExtra("message", message);
            this.startService(intent);
        } else if (messageEvent.getPath().equals(STOP_SERVICE)){
            Log.v(TAG, STOP_SERVICE);
            this.stopService(new Intent(getApplicationContext(), WearGestureService.class));
        } else {
            super.onMessageReceived(messageEvent);
        }
    }

}
