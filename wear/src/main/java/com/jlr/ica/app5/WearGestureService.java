package com.jlr.ica.app5;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by luke on 16/03/2016.
 * Monitors wearable accelerometer and sends trigger to phone when gesture detected.
 */
public class WearGestureService extends Service implements SensorEventListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    public static final String TAG = "WearGestureService";
    public static final String MESSAGE_TAG = "/gesture";
    private static final long CONNECTION_TIME_OUT_MS = 100;

    GoogleApiClient mGoogleApiClient;
    private String nodeId;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private Sensor senGravity;


    private Double[] a = new Double[]{0.0,0.0,0.0};
    private Double[] g = new Double[]{0.0,0.0,0.0};
    private ArrayList<Double[]> aCont = new ArrayList<>();
    private ArrayList<Double> dtCont = new ArrayList<>();
    private Double[] gPre = new Double[]{0.0,0.0,0.0};
    private Double[] avA = new Double[]{0.0,0.0,0.0};

    double aFilt = 0.3;

    private long systemTime = 0;
    private long prevAccelTime = 0;

    long prevPos = 0;
    long prevNeg = 0;
    long prevUp = 0;
    long prevDown = 0;
    long prevGesture = 0;
    long gestureDelay = 200;
    long gesturePostDelay = 100;
    long gestureHysteresis = 700;
    double gestureAccelSensitivity = 2;
    int nHistory = 20;
    GestureClass gestureDir = GestureClass.UNSET;

    enum GestureClass {
        UNSET,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }


    ArrayList<String> appList = new ArrayList<>();
    int currentApp = 0;

    PowerManager.WakeLock wl;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // Get gesture sensitivity from intent
        String message = intent.getStringExtra("message");
        Log.v(TAG, "received message: " + message);
        gestureAccelSensitivity = Double.parseDouble(message);

        // Register wearable api
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        // Show notification to allow service to run in foreground
        Intent notificationIntent = new Intent(this, MessageActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("MySpin Gesture Control")
                .setContentText("Gestures are being monitored")
                .setSmallIcon(R.drawable.gesture_icon_small)
                .addAction(R.drawable.cross, "Stop service", pendingIntent)
                .build();
        startForeground(101, notification);

        // Set up sensors
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        senGravity = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senGravity, SensorManager.SENSOR_DELAY_FASTEST);

        // Stop wearable from sleeping
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PWLGestureService");
        wl.acquire();

        // Return as sticky, again stops android closing service
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        senSensorManager.unregisterListener(this);
        mGoogleApiClient.disconnect();
        wl.release();
    }

    /**
     * Detects gestures live.
     * Sends data to checkGesture() for further analysis
     * @param sensorEvent
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        // raw accelerometer, used for determining orientation
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            g[0] = ((double) sensorEvent.values[0])-a[0];// * (1 - aFilt) + g[0] * aFilt;
            g[1] = ((double) sensorEvent.values[1])-a[1];// * (1 - aFilt) + g[1] * aFilt;
            g[2] = ((double) sensorEvent.values[2])-a[2];// * (1 - aFilt) + g[2] * aFilt;
        }

        // linear acceleration. Processed stream indicating acceleration minus gravity
        if (mySensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            // Log current time and determine time since last data time
            prevAccelTime = systemTime;
            systemTime = System.currentTimeMillis();
            long dT = systemTime - prevAccelTime;
            Double dt = ((double)dT)/1000.0;

            // record acceleration and apply simple low pass filter
            a[0] = ((double) sensorEvent.values[0])*(1-aFilt) + a[0]*aFilt;
            a[1] = ((double) sensorEvent.values[1])*(1-aFilt) + a[1]*aFilt;
            a[2] = ((double) sensorEvent.values[2])*(1-aFilt) + a[2]*aFilt;

            // add latest data entry to data history
            aCont.add(a.clone());
            dtCont.add(dt);

            // wait for data history to be sufficiently large
            if (aCont.size()>nHistory) {
                aCont.remove(0);
                dtCont.remove(0);

                // only procede to detect gesture if there hasn't been a gesture within specified time
                // stops false positives and double readings
                if (systemTime-prevGesture>gestureHysteresis) {
                    // calculate average acceleration over recorded history
                    avA = getAverageAccel();
                    // Check if instantaneous acceleration is significantly greater than average
                    // Do this in four directions: Left, Right, Up, Down;
                    if (checkPosAccel()) {
                        prevPos = systemTime; // Record trigger time
                        if (prevPos - prevNeg < gestureDelay) { // if there has been sufficient delay
                            gestureDir = GestureClass.RIGHT; // set gesture
                            prevGesture = systemTime; // log gesture time
                        }
                        gPre = g.clone(); // log linear acceleration at time of first trigger
                    }
                    if (checkNegAccel()) {
                        prevNeg = systemTime;
                        if (prevNeg - prevPos < gestureDelay) {
                            gestureDir = GestureClass.LEFT;
                            prevGesture = systemTime;
                        }
                        gPre = g.clone();
                    }
                    if (checkUpAccel()) {
                        prevUp = systemTime;
                        if (prevUp - prevDown < gestureDelay) {
                            gestureDir = GestureClass.DOWN;
                            prevGesture = systemTime;
                        }
                        gPre = g.clone();
                    }
                    if (checkDownAccel()) {
                        prevDown = systemTime;
                        if (prevDown - prevUp < gestureDelay) {
                            gestureDir = GestureClass.UP;
                            prevGesture = systemTime;
                        }
                        gPre = g.clone();
                    }
                } else if (systemTime-prevGesture>gesturePostDelay){ // wait some time to allow linear acceleration to settle
                    if (gestureDir!=GestureClass.UNSET)
                        checkGesture(gestureDir); // Send gesture to post processing
                }
            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void checkGesture(GestureClass gDir){
        Log.d(TAG, "gPre:" + arrayToString(gPre));
        Log.d(TAG, "gPost:"+arrayToString(g));
        for (int i = 0; i < aCont.size(); i++) {
            Log.d(TAG, "aCont"+arrayToString(aCont.get(i)));
        }
        switch (gDir){
            case UNSET:
                break;
            case UP:
                gestureUp();
                break;
            case DOWN:
                gestureDown();
                break;
            case LEFT:
                gestureLeft();
                break;
            case RIGHT:
                if (g[0]>4){ // watch is close to vertical then record a mute gesture
                    gestureMute();
                } else {
                    gestureRight();
                }
                break;
        }
        gestureDir = GestureClass.UNSET; // reset gesture state
    }

    public void gestureLeft(){
        Log.d(TAG, "Gesture Left");
        sendToMobile(MESSAGE_TAG, "L");
    }

    public void gestureRight(){
        Log.d(TAG, "Gesture Right");
        sendToMobile(MESSAGE_TAG, "R");
    }
    public void gestureMute(){
        Log.d(TAG, "Gesture Mute");
        sendToMobile(MESSAGE_TAG, "M");
    }
    public void gestureUp(){
        Log.d(TAG, "Gesture Up");
        sendToMobile(MESSAGE_TAG, "U");
    }
    public void gestureDown(){
        Log.d(TAG, "Gesture Down");
        sendToMobile(MESSAGE_TAG, "D");
    }



    public boolean checkPosAccel(){
        return aCont.get(aCont.size()-1)[1]>avA[1]+ gestureAccelSensitivity;
    }

    public boolean checkNegAccel(){
        return aCont.get(aCont.size()-1)[1]<avA[1]- gestureAccelSensitivity;
    }

    public boolean checkUpAccel(){
        return aCont.get(aCont.size()-1)[2]>avA[2]+ gestureAccelSensitivity;
    }
    public boolean checkDownAccel(){
        return aCont.get(aCont.size()-1)[2]<avA[2]- gestureAccelSensitivity;
    }

    public Double[] getAverageAccel(){
        Double[] sumA = new Double[]{0.0,0.0,0.0};
        int n = aCont.size()-1;
        for (int i = 0; i < n; i++) {
            sumA[0] += aCont.get(i)[0];
            sumA[1] += aCont.get(i)[1];
            sumA[2] += aCont.get(i)[2];
        }
        sumA[0] *= 1/n;
        sumA[1] *= 1/n;
        sumA[2] *= 1/n;
        return sumA;
    }

    private String arrayToString(Double[] arr){
        DecimalFormat df = new DecimalFormat("0.0");
        return "["+df.format(arr[0])+","+df.format(arr[1])+","+df.format(arr[2]) + "]";
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v(TAG, "Connected");

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG, "Connection Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v(TAG, "Connection Failed");
    }

    public void sendToMobile(final String path, final String message){

                if (mGoogleApiClient.isConnected()) {

                    PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(path);
                    // Include time to force data layer to update
                    putDataMapRequest.getDataMap().putString("gesture", message + System.currentTimeMillis());
                    putDataMapRequest.setUrgent();
                    Wearable.DataApi.putDataItem(mGoogleApiClient, putDataMapRequest.asPutDataRequest());
                    Log.v(TAG, "Sent Item");

                    // Vibrate watch as feedback
                    Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                    long[] vibrationPattern = {0, 200, 0};
                    final int indexInPatternToRepeat = -1;
                    vibrator.vibrate(vibrationPattern, indexInPatternToRepeat);
                } else {
                    Log.e(TAG, "Failed to send gesture to phone - Client disconnected from Google Play "
                            + "Services");
                }
    }


}
